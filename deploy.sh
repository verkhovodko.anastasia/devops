docker login registry.gitlab.com -u "$1" -p "$2"
docker-compose -f ~/fromjenkins/docker-compose-prod.yaml pull
docker-compose -f ~/fromjenkins/docker-compose-prod.yaml up -d db
docker-compose -f ~/fromjenkins/docker-compose-prod.yaml run --rm web python manage.py migrate
docker-compose -f ~/fromjenkins/docker-compose-prod.yaml up -d --force-recreate
docker logout