pipeline {
  environment {
    registry = "registry.gitlab.com"
    registryCredential = credentials('registry.gitlab.com')
    prodIP = credentials('prod_server')
  }
  agent any
  stages {
    stage('Cloning Git') {
      steps {
        git 'git@gitlab.com:verkhovodko.anastasia/devops.git'
      }
    }
    stage('Building image') {
      steps{
        script {
          sh "docker-compose build"
        }
      }
    }
    stage('Run test') {
      steps{
        script {
          sh """
          docker-compose run --rm web bash -c "sleep 10s && python manage.py test"
          docker-compose down
          """
        }
      }
    }  
    stage('Docker login') {
      when {
                branch 'master'
            }
      steps{
        script {
          sh """
          echo "docker login..."
          docker login $registry -u $registryCredential_USR -p $registryCredential_PSW
          """
        }
      }
    }
    stage('Docker push') {
      when {
                branch 'master'
            }
      steps {
        script {
          sh "docker-compose push"
        }
      }
    }
    
    stage('Docker logout') {
      when {
                branch 'master'
            }
      steps{
        script {
          sh """
          docker logout $registry
          """
        }
      }
    }
    stage('Install docker-ce and docker-compose') {
      when {
                branch 'master'
            }
      steps{
        script {
          sh """
          ansible-playbook ~/docker/site.yaml -i ~/docker/hosts.yaml
          """
        }
      }
    }
    stage('Copy files via ssh') {
      when {
                branch 'master'
            }
      steps{
        script {
          sh """
          scp /var/lib/jenkins/workspace/build_image_from_gitlab/docker-compose-prod.yaml $prodIP:~/fromjenkins/docker-compose-prod.yaml
          scp /var/lib/jenkins/workspace/build_image_from_gitlab/deploy.sh $prodIP:~/fromjenkins/deploy.sh
          """
        }
      }
    }
    stage('Run deploy.sh') {
      when {
                branch 'master'
            }
      steps{
        script {
          sh """
          ssh $prodIP 'sh ~/fromjenkins/deploy.sh "$registryCredential_USR" "$registryCredential_PSW"'
          """
        }
      }
    }
  }
    post {
            success {
                slackSend (color: '#00FF00', message: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
            }
            failure {
                slackSend (color: '#FF0000', message: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
            }
    }
}